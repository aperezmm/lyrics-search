
const colors = require('tailwindcss/colors')


module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      transitionDuration: {
        '0': '0ms',
        '100': '100ms',
        '200': '200ms',
        '300': '300ms',
        '400': '400ms',
        '500': '500ms',
        '600': '600ms',
        '700': '700ms',
        '800': '800ms',
        '900': '900ms',
        '1000': '1000ms',
        '1500': '1500ms',
        '2000': '2000ms',
      }
    },
    screens: {
      xxs: '0px',  // 0   - 359
      xs: '360px', // 360 - 599
      sm: '600px', // 600 - 959
      md: '960px', // 960 - 1279
      lg: '1280px',// 1280 - 1599
      xl: '1600px',// 1600 - 1919
      "2xl": '1920px', // 1920 ....
      print: { 'raw': 'print' }
    },
    colors: {
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      red: colors.red,
      green: colors.green,
      blue: colors.blue,
      blueGray: colors.blueGray,
      cyan: colors.cyan,
      sky: colors.sky,
      indigo: colors.indigo,
      violet: colors.violet
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
