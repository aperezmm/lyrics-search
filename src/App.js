import React, {useEffect} from 'react';
import './App.css';

/**COMPONENTS */
import { NewNote } from './components/notes/NewNote';
import { ListNotes } from './components/notes/ListNotes';

/**STORE */
// import { store } from './initialStore';

import {useDispatch} from 'react-redux';

/**REDUX ACTIONS */
import { initNotes } from './components/notes/store/noteActions';

export const App = () => {

  const dispatch = useDispatch();

  const filterSelected = value => {
    console.log(value);
  }

  /**CUANDO LA APP INICIE, INICIA EL ESTADO */
  useEffect(() => {
    console.log("Voy a traer datos de la API");
    dispatch(initNotes());
    //No hay necesidad de pasarle nada porque el middleware hace eso.
  },[dispatch])

  return ( 
    <div className="w-full text-center text-2xl">
      <NewNote></NewNote>
      <hr></hr>
      <div>
        TODOS
          <input type='radio' name='filter' onChange={ () => filterSelected('ALL')}/>

        IMPORTANT
          <input type='radio' name='filter' onChange={ () => filterSelected('IMPORTANT')}/>   

        NO_IMPORTANT
          <input type='radio' name='filter' onChange={ () => filterSelected('NO_IMPORTANT')}/>
      </div>
      <hr></hr>
      <ListNotes></ListNotes>      
    </div>
  )
}
