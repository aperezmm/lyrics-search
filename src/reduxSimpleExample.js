import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';

/** REDUX */
import { createStore } from 'redux';

// ACTIONS REDUX   
const actionIncremented = {
  type: '@counter/incremented'
};

const actionDecremented = {
  type: '@counter/decremented'
}

const actionReset = {
  type: '@counter/reseted'
}

// REDUCER COUNTER
const counterReducer = (state = 0, action) => {

  console.log(action);
  switch (action.type) {
    case '@counter/incremented':
      return state + 1;
    
    case '@counter/decremented':
      return state - 1;
    
    case '@counter/reseted':
      return 0;
  
    default:
      return state;
  }
}

// CREAMOS LA STORE
const storeCounter = createStore(
  counterReducer,
  window._REDUX_DEVTOOLS_EXTENSION_ && window._REDUX_DEVTOOLS_EXTENSION_()
); 

const App = () => {
  return (
    <div className="w-full text-center text-2xl">
      <div>
        {storeCounter.getState()}
      </div>

      <div className="flex justify-center">
        <div className="p-2 border-2 bg-gray-100">
          {/* YO QUIERO QUE ME INCREMENTE */}
          <button onClick={() => storeCounter.dispatch(actionIncremented)}>+</button>
        </div>
        <div className="p-2 border-2 bg-gray-100">
          {/* YO QUIERO QUE ME DISMINUYA */}
          <button onClick={() => storeCounter.dispatch(actionDecremented)}>-</button>
        </div>
        <div className="p-2 border-2 bg-gray-100">
          {/* YO QUIERO QUE ME RESET */}
          <button onClick={() => storeCounter.dispatch(actionReset)}>Reset</button>
        </div>
      </div>
    </div>
  )
}

const renderApp = () => {
    ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.getElementById('root')
  );
}

renderApp(); 
// nos subscribimos a los cambios del store y cada vez que cambie hacemos una renderización.
storeCounter.subscribe(renderApp);

