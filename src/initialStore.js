/**REDUX REDUCERS */
import { noteReducer } from './components/notes/store/noteReducer';
import { filterReducer } from './components/notes/store/filterReducer';

/** REDUX */
import { createStore, combineReducers, applyMiddleware } from 'redux';

/**TOOLS */
import { composeWithDevTools } from 'redux-devtools-extension'

/**MIDDLEWARE */
import thunk from 'redux-thunk';


const reducer = combineReducers({
    notes: noteReducer,
    filter: filterReducer,
  })
  
// STORE DE LAS NOTAS
export const store = createStore(
    reducer, 
    composeWithDevTools(
      applyMiddleware(thunk)
    )
);

//Si no tuvieramos combineReducers
// export const store = createStore(
//   reducer, 
//   applyMiddleware(thunk)
  
// );



