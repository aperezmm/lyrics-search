/**REDUX REDUCERS */
import { useDispatch } from 'react-redux';
import { createNote } from './store/noteActions';

export function NewNote(){

    const dispatch = useDispatch();

    const addNote = async (event) => {
        event.preventDefault();
        const {target} = event;
        const content = target.noteName.value
    
        target.noteName.value = '';
    
        // storeNotes.dispatch({
        //   type: '@notes/created',
        //   payload: {
        //     content,
        //     important: false,
        //     id: generateId()
        //   }
        // })

        dispatch(createNote(content));
    }

    return (
        <div className="m-2">
            <form onSubmit={addNote}>
            <input type="text" name="noteName" className="border-2 bg-gray-100"/>
            <button className="border-2 bg-gray-100">ADD</button>
            </form>
        </div>
    )
}