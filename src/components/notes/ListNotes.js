/**REDUX ACTIONS */
import { toggleImportanceOf } from "./store/noteActions";

/**REACT-REDUX */
import { useDispatch, useSelector } from "react-redux";

export function ListNotes() {
    const dispatch = useDispatch();

    /**Actualmente nos interesa todo el estado */
    /**Nos subscribimos y obtenemos lo que nos interesa */
    const stateNotes = useSelector((state) => state.notes);

    /**Obtener las notas importantes */
    const importantNotes = useSelector((state) => state.notes.filter((note) => note.important));

    const toggleImportanceNote = (id) => {
        dispatch(toggleImportanceOf(id));
    };

    console.log({ STATENOTES: stateNotes, IMPORTANTNOTES: importantNotes });

    return (
        <div className="flex flex-col justify-center">
            {stateNotes.map((stateNote) => {
                return (
                    <div key={stateNote.id} className="">
                        <div>ID: {stateNote.id}</div>
                        <div>Content: {stateNote.content}</div>
                        <div className="cursor-pointer" onClick={() => toggleImportanceNote(stateNote.id)}>
                            {stateNote.important ? "Es importante" : "No es importante"}
                        </div>
                    </div>
                );
            })}
        </div>
    );
}
