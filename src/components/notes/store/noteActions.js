import { getAll, createNewNote } from '../../../components/services/notesService';

export const generateId = () => {
    return Math.floor(Math.random() * 999 + 1)
};
  
  // CREAMOS LA ACCIÓN PARA CREAR NOTAS
export const createNote = (content) => {
    return async (dispatch) => {
        //no debería tener logica de negocio
        //solo lógica de estado de la APP
        const newNote = await createNewNote(content);

        dispatch({
            type: '@notes/created',
            payload: newNote
        })
    }
}

/**ACTIONS CREATOS
 * retorna un objeto
 */
export const toggleImportanceOf = (id) => {
    return {
        type: '@note/toggle_importance',
        payload: {
        id
        }
    }
}

/**THUNK ACTIONS 
 * devuelve un función asincrona
*/
export const initNotes = () => {
    return async (dispatch) => {
        //dispatch para saber cuando debe disparar la acción

        //no debería tener lógica de negocio acá
        //solo lógica de estado de la APP

        const notes = await getAll();
        //Una vez que tenemos las notas hacemos un dispatch de la acción
        
        dispatch({
            type: '@notes/init',
            payload: notes
        })
        // return {
        //     type: '@notes/init',
        //     payload: notes
        // }
    }
    
}