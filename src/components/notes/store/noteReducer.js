// REDUCER DE LAS NOTAS
// Siempre debe devolver la misma información, el mismo tipo de información.
// Función PURA



export const noteReducer = (state = [] , action) => {
    switch (action.type) {

        case '@notes/init':
            console.log("Vamos a iniciar con esta información");
            return action.payload;

        case '@notes/created':
            // return state.concat(action.payload);  
            // express operator
            return [...state, action.payload];
        
        case '@note/toggle_importance':

            //NO SE DEBERÍA MUTAR EL OBJETO
            const {id} = action.payload;
            return state.map(note => {
                if(note.id === id) {
                    // note.important = !note.important;

                    // PARA EVITAR MUTACIÓN
                    return {
                        ...note,
                        important: !note.important
                    }
                }
                return note;
            })
        
        default:
            return state
    }
}