// Craco config.js
module.exports = {
    webpack: {
        headers: {
            'X-Frame-Options': 'Deny'
        }
    },
    style: {
        postcss: {
            plugins: [
                require('tailwindcss'),
                require('autoprefixer'),
            ],
        },
    },
    webpack: {
        headers: {
            'X-Frame-Options': 'Deny'
        }
    }
}